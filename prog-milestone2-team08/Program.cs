﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace program
{
    class program
    {
       
        static void Main(string[] args)
        {
            var ansMenu = 0;
            var returnMenu = "";

            try
            {
                do
                {
                    Console.WriteLine("-----  Menu    -----");
                    Console.WriteLine("1. Date Calc   -----");
                    Console.WriteLine("2. Grade Calc  -----");
                    Console.WriteLine("3. Number Game -----");
                    Console.WriteLine("4. Food list   -----");
                    Console.WriteLine("5. Exit        -----");
                    Console.WriteLine("--------------------");
                    Console.WriteLine();
                    Console.WriteLine("Please enter a selection: ");
                    ansMenu = int.Parse(Console.ReadLine());
                    Console.Clear();

                    switch (ansMenu)
                    {
                        case 1:
                            try
                            {
                                Console.WriteLine("----   Date Calc Menu   ----");
                                Console.WriteLine("1. Date of birth   ---------");
                                Console.WriteLine("2. Years old       ---------");
                                Console.WriteLine("----------------------------");
                                Console.WriteLine("Please enter a selection: ");
                                var ansDate = int.Parse(Console.ReadLine());
                                Console.Clear();

                                switch (ansDate)
                                {
                                    case 1:
                                        dateDays();
                                        break;
                                    case 2:
                                        dateYears();
                                        break;
                                }

                                Console.WriteLine("Press [m/M] to return to menu..");
                                returnMenu = Console.ReadLine();
                                Console.Clear();
                            }
                            catch
                            {

                            }
                            break;
                        case 2:
                            treye();
                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                        case 3:
                            random();
                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            foodMenu();
                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                        case 5:
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Please enter a selection.");
                            Console.WriteLine("Press [m/M] to return to menu..");
                            returnMenu = Console.ReadLine();
                            Console.Clear();
                            break;
                    }

                } while (returnMenu == "m" || returnMenu == "M");
            }
            catch
            {

            }
        }

        static void dateDays()
        {
            var today = DateTime.Today;

            Console.WriteLine("Please enter your date of birth (dd-mm-yyyy): ");
            var dob = Console.ReadLine();

            DateTime birthDate = DateTime.Parse(dob);

            TimeSpan elapsed = today.Subtract(birthDate);

            double daysAgo = elapsed.TotalDays;
            Console.WriteLine($"You're {daysAgo} days old.");
        }
        static void dateYears()
        {
            Console.WriteLine("Please enter your age (e.g. 26): ");
            var ansYears = int.Parse(Console.ReadLine());

            DateTime current = DateTime.Today;
            var a = current.AddYears(-ansYears);

            TimeSpan elapsed = current.Subtract(a);

            double days = elapsed.TotalDays;
            Console.WriteLine($"There are {days} days in {ansYears} years.");
        }
        static List<int> result = new List<int>();
        static void random()
        {

            try
            {
                int score = 0;
                int i = 0;

                do
                {

                    int number = 0;

                    Random rnd = new Random();
                    int Value = rnd.Next(1, 6);

                    Console.WriteLine("--------------------------------");
                    Console.WriteLine($"Your Current Score Is {score}");
                    Console.WriteLine("--------------------------------");
                    Console.WriteLine("Please guess a number between 1-5");

                    string text1 = "x";
                    int num1;

                    bool number1 = int.TryParse(text1, out num1);
                    number = int.Parse(Console.ReadLine());
                    Console.WriteLine($"You entered {number} and the number was {Value}");

                    Console.Clear();
                    if (number1 == false)
                    {

                        if
                            (number == Value)
                            score++;
                        i++;
                    }

                } while (i <= 4);


                result.Add(score);
                Console.Clear();
                Console.WriteLine($"Well done you made it to the end and you got {score} out of 5 ");
                Console.WriteLine("-----------------------------------------------------------------");

                results();
                finish();
            }
            catch
            {

            }

        }

        static void finish()
        {
            Console.WriteLine("-----------------------------------------------------------------");
            Console.WriteLine("Would you like to play again (y/n)");
            string question = Console.ReadLine();

            if (question.Equals("y"))
            {
                Console.Clear();

                random();

            }
            else if (question.Equals("n"))
            {
                Console.WriteLine("Why you no stay for more games");

            }

        }
        static void results()
        {
            foreach (int i in result)
            {
                Console.WriteLine($"Your previous score was [{i}]");
            }

        }
        static void foodMenu()
        {
            var submenu = "";

            do

            {                
                Console.Clear();
                Console.WriteLine("      Food menu      ");
                Console.WriteLine("                     ");
                Console.WriteLine("-- 1. Enter foods  --");
                Console.WriteLine("-- 2. Show list    --");
                Console.WriteLine("-- 3. change       --");
                Console.WriteLine("-- 4. End          --\n");
                Console.WriteLine("");
                Console.WriteLine("Please select a number: ");
                
                
                String input = Console.ReadLine();
                int selectedOption;
                
                if (int.TryParse(input, out selectedOption))

                {
                    switch (selectedOption)
                    {
                        case 1:
                            foods();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                        case 2:
                            show(food);
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                        case 3:
                            change();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                        case 4:
                            end();
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                        default:
                            Console.WriteLine("Press r/R for to return to sub menu or press Enter for main menu selection\n");
                            submenu = Console.ReadLine();
                            break;
                    }

                }

            } while (submenu == "r" || submenu == "R");
            
            }

        static Dictionary<int, string> food = new Dictionary<int, string>();

        static Dictionary<int, string> foods()
        {            
            Console.WriteLine("Please enter 5 of your favorite foods in order of favorite to least favorite.\n");
            
            check();
           
            return food;
        }

        static void show(Dictionary<int,string> food)
        {
               Console.WriteLine("Your favorite {0} foods in order are:  \n",food.Count);
            
            foreach (KeyValuePair<int, string> f in food)
            {
                Console.WriteLine(f);
            }
        }
        
        static void change()
        {
            try
            {
                var fooditem = 0;

                show(food);

                Console.WriteLine("What item of food would you like to change? (Select the number, press enter then type in the new item\n)");
                fooditem = int.Parse(Console.ReadLine());

                if (fooditem == 1)
                {
                    food.Remove(1);
                    food.Add(1, Console.ReadLine());

                }
                if (fooditem == 2)
                {
                    food.Remove(2);
                    food.Add(2, Console.ReadLine());
                }
                if (fooditem == 3)
                {
                    food.Remove(3);
                    food.Add(3, Console.ReadLine());
                }
                if (fooditem == 4)
                {
                    food.Remove(4);
                    food.Add(4, Console.ReadLine());
                }
                if (fooditem == 5)
                {
                    food.Remove(5);
                    food.Add(5, Console.ReadLine());
                }

                show(food);

            }
            
            catch
            {

            }
            
        }
         static void check()
        {
            string input = "";
            int value = 1;

            while (food.Count != 5)
            {
                input = Console.ReadLine();

                if (food.Count == 0)
                {
                    food.Add(value, input);
                }
                else
                {
                    if (food.ContainsValue(input))
                    {
                        Console.WriteLine("That is already there");
                    }
                    else
                    {
                        food.Add(value, input);
                        Console.WriteLine("Food Added");
                    }

                }

                value++;

            }
        }
        
        static void end()
        {
            Console.WriteLine("Thanks for taking part press Enter for the menu.\n");
        }

        //everything below this comment is Treye's work.
        static List<int> score = new List<int>();

        public static void treye()
        {
            Console.WriteLine("--------------------------------------");
            Console.WriteLine();
            Console.WriteLine("Welcome, please enter your Student ID.");
            Console.WriteLine();
            Console.WriteLine("--------------------------------------");

            string ID = Console.ReadLine();

            //Welcome the user. Ask if they are level 5 or 6
            Console.WriteLine("Welcome {0}! If you are Level 5, press 5. If you are Level 6, press 6.", ID);

            int lvl5paper1, lvl5paper2, lvl5paper3, lvl5paper4, lvl6paper1, lvl6paper2, lvl6paper3;
            int result;
            double average;
            if (int.TryParse(Console.ReadLine(), out result))


            {

                if (result == 5)
                {

                    //Ask for level 5 students paper codes and marks
                    Console.WriteLine("Level 5 student, please enter your 4 paper scores below.");

                    Console.WriteLine("Please enter your first paper code");
                    string lvl5code1 = Console.ReadLine();
                    lvl5paper1 = GetMarkInput("first", out lvl5paper1);
                    Console.WriteLine("Please enter your second paper code");
                    string lvl5code2 = Console.ReadLine();
                    lvl5paper2 = GetMarkInput("second", out lvl5paper2);
                    Console.WriteLine("Please enter your third paper code");
                    string lvl5code3 = Console.ReadLine();
                    lvl5paper3 = GetMarkInput("third", out lvl5paper3);
                    Console.WriteLine("Please enter your fourth paper code");
                    string lvl5code4 = Console.ReadLine();
                    lvl5paper4 = GetMarkInput("fourth", out lvl5paper4);

                    average = (lvl5paper1 + lvl5paper2 + lvl5paper3 + lvl5paper4) / 4;

                    var letterGrade = " ";


                    if (average > 90)
                        letterGrade = "A+ (Pass)";
                    else
                        if (average > 85)
                        letterGrade = "A (Pass)";
                    else
                        if (average > 80)
                        letterGrade = "A- (Pass)";
                    else
                        if (average > 75)
                        letterGrade = "B+ (Pass)";
                    else
                        if (average > 70)
                        letterGrade = "B (Pass)";
                    else
                        if (average > 65)
                        letterGrade = "B- (Pass)";
                    else
                        if (average > 60)
                        letterGrade = "C+ (Pass)";
                    else
                        if (average > 55)
                        letterGrade = "C (Pass)";
                    else
                        if (average > 50)
                        letterGrade = "C- (Pass)";
                    else
                        if (average > 40)
                        letterGrade = "D (Fail)";
                    else
                        if (average <= 39)
                        letterGrade = "E (Fail)";

                    Console.WriteLine($"Your student ID is {ID}, your study level is {result}, your final percentage is {average} and your final grade is {letterGrade}");


                    List<int> score = new List<int>();
                    if (lvl5paper1 > 85)
                    {
                        score.Add(1);
                    }
                    if (lvl5paper2 > 85)
                    {
                        score.Add(1);
                    }
                    if (lvl5paper3 > 85)
                    {
                        score.Add(1);
                    }
                    if (lvl5paper4 > 85)
                    {
                        score.Add(1);
                    }

                    Console.WriteLine($"You have {score.Count} papers scored over 85% (A+)");

                }

                else if (result == 6)
                {

                    //Ask for level 6 paper codes and marks
                    Console.WriteLine("Level 6 student, please enter your 3 paper scores below.");

                    Console.WriteLine("Please enter your first paper code");
                    string lvl6code1 = Console.ReadLine();
                    lvl6paper1 = GetMarkInput("first", out lvl6paper1);
                    Console.WriteLine("Please enter your second paper code");
                    string lvl6code2 = Console.ReadLine();
                    lvl6paper2 = GetMarkInput("second", out lvl6paper2);
                    Console.WriteLine("Please enter your third paper code");
                    string lvl6code3 = Console.ReadLine();
                    lvl6paper3 = GetMarkInput("third", out lvl6paper3);

                    average = (lvl6paper1 + lvl6paper2 + lvl6paper3) / 3;

                    var letterGrade = " ";

                    if (average > 90)
                        letterGrade = "A+ (Pass)";
                    else
                        if (average > 85)
                        letterGrade = "A (Pass)";
                    else
                        if (average > 80)
                        letterGrade = "A- (Pass)";
                    else
                        if (average > 75)
                        letterGrade = "B+ (Pass)";
                    else
                        if (average > 70)
                        letterGrade = "B (Pass)";
                    else
                        if (average > 65)
                        letterGrade = "B- (Pass)";
                    else
                        if (average > 60)
                        letterGrade = "C+ (Pass)";
                    else
                        if (average > 55)
                        letterGrade = "C (Pass)";
                    else
                        if (average > 50)
                        letterGrade = "C- (Pass)";
                    else
                        if (average > 40)
                        letterGrade = "D (Fail)";
                    else
                        if (average <= 39)
                        letterGrade = "E (Fail)";

                    Console.WriteLine($"Your student ID is {ID}, your study level is {result}, your final percentage is {average} and your final grade is {letterGrade}");


                    if (lvl6paper1 > 85)
                    {
                        score.Add(1);
                    }
                    if (lvl6paper2 > 85)
                    {
                        score.Add(1);
                    }
                    if (lvl6paper3 > 85)
                    {
                        score.Add(1);
                    }

                    Console.WriteLine($"You have {score.Count} papers scored over 85% (A+)");
                }

                else

                    Console.WriteLine("Sorry, bad input");
                Console.ReadKey();
                Console.Clear();

            }

            else

                Console.WriteLine("Sorry, bad input");

            Console.ReadLine();

        }
        public static double CalcAvg(int lvl5paper1, int lvl5paper2, int lvl5paper3, int lvl5paper4)

        {
            return (lvl5paper1 + lvl5paper2 + lvl5paper3 + lvl5paper4) / 4;
        }


        public static double CalcAvg2(int lvl6paper1, int lvl6paper2, int lvl6paper3)

        {
            return (lvl6paper1 + lvl6paper2 + lvl6paper3) / 3;
        }

        static void scoreresults()
        {
            foreach (int i in score)
            {
                Console.WriteLine("These are your results");
            }
        }


        public static int GetMarkInput(string Order, out int t)

        {
            string inValue;
            Console.WriteLine("Enter the mark for your {0} paper out of 100: ", Order);
            inValue = Console.ReadLine();
            t = Convert.ToInt32(inValue);
            return t;
        }



    }
}